
# Generates a random number and displays in format "Name GeneratedNumber"
def pseudo_num name
  num = 6 + rand(10)
  puts "#{name} #{num}"
end

pseudo_num 'Rashmi'

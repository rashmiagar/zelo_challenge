In the programming language that is specified in this job's description, write simple, commented code for a function (or method) that takes a variable name as parameter.
Generate a random number between 6 and 15 and return the result in this format: “Name GeneratedNumber”. Eg: “Rahul 11”


The logic is that rand(num) method generates a number between 0 and num-1.  Since we want the random number to be higher than 6, we need to modify the left range by 6, so I added 6, this will generate numbers between 6 and num+6. Because the higher bound we want is 15, the parameter to rand() needs to be 5 less 15, 10 (since its non-inclusive).